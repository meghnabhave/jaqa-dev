
import json
from tokenizer_keras import tokenize
from retriever import retrieveTopNDocs, retrieveTopNParagraphs
from pprint import pprint


if __name__ == '__main__':

    with open('term_count.json') as json_file:
        tokenizer_output = json.load(json_file)
    with open('term_dict') as json_file:
        term_dict = json_file.read().split(",")
    with open('tfidf.json') as json_file:
        tfidf = json.load(json_file)

    query = input("Enter a query: ")
    tokenized_query = tokenize("question",query)

    doc_scores = retrieveTopNDocs(tfidf,term_dict,tokenized_query)

    para_score_list,para_text_list = retrieveTopNParagraphs(tokenized_query, tokenizer_output, doc_scores)


    for i in range(0,len(para_score_list)):
        print(para_score_list[i])
        print(para_text_list[i])
        print("\n")
