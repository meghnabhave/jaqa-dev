from multiprocessing import Pool
import json
from itertools import repeat
from utils import build_count_matrix, build_tfidf_matrix
import numpy as np
from math import ceil
from tokenizer_keras import tokenize

if __name__ == '__main__':

    # tokenize documents in corpus and creates files term_count.json and term_dict
    tokenize("doc")

    with open('term_count.json') as json_file:
        tokenizer_output = json.load(json_file)
    with open('term_dict') as json_file:
        term_dict = json_file.read().split(",")

    #calculate number of processes dynamically depending on number of documents
    number_of_documents_in_a_thread = 3
    total_docs = len(tokenizer_output)
    number_of_processes = ceil(total_docs/number_of_documents_in_a_thread)

    vocab_size = len(term_dict)

    # Count Matrix Creation
    count_matrix = np.zeros((vocab_size,total_docs))
    pool = Pool(processes=number_of_processes)              # start worker processes

    step = max(number_of_documents_in_a_thread, 1)
    batches = [tokenizer_output[i:i + step] for i in range(0, total_docs, step)]

    for c in pool.starmap(build_count_matrix,zip(repeat(count_matrix),batches,repeat(term_dict))):
        count_matrix=count_matrix+c

    pool.close()
    pool.join()

    # TFIDF Dictionary Creation
    tfidf = build_tfidf_matrix(count_matrix)

    # Write TFIDF Dictionary to file
    json = json.dumps(tfidf)
    f = open("tfidf.json","w")
    f.write(json)
    f.close()
