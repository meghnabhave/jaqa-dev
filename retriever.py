from multiprocessing import Pool
import json
from itertools import repeat
from utils import build_query_vector, build_tfidf_matrix, get_closest_docs
import numpy as np
from pprint import pprint
from tokenizer_keras import tokenize


def retrieveTopNDocs(tfidf,term_dict,query):
    query_vector = build_query_vector(query, term_dict)
    doc_scores = get_closest_docs(tfidf, query_vector)

    n = 0
    while(n < 5 and doc_scores[n][1] != 0.0):
        n=n+1

    return doc_scores[0:n]

def retrieveTopNParagraphs(query, tokenizer_output, doc_scores):

    paragraphs = dict()
    para_scores = dict()

    for doc_id,score in doc_scores:
        for para_id, object in tokenizer_output[int(doc_id)][str(doc_id)].items():
            paragraphs[(doc_id,int(para_id))] = object
            para_scores[(doc_id,int(para_id))] = score

    # Build term dictionary
    para_dict = set()
    for para_id, para in paragraphs.items():
        para_dict.update(list(para['tokens'].keys()))
    para_dict = list(para_dict)
    para_dict.sort()
    # Build count matrix
    para_ids = list(paragraphs.keys())

    count_matrix = np.zeros((len(para_dict),len(paragraphs)))

    for para_id, para_object in paragraphs.items():
        for term,freq in para_object['tokens'].items():
            count_matrix[para_dict.index(term),para_ids.index(para_id)] = freq

    tfidf_matrix = build_tfidf_matrix(count_matrix)

    query_vector = build_query_vector(query, para_dict)

    for para_id,para_vector in tfidf_matrix.items():
        para_scores[para_ids[para_id]] = para_scores[para_ids[para_id]] + np.dot(para_vector,query_vector)*-1

    sorted_para_scores = sorted(para_scores.items(), key=lambda x: x[1])

    para_text_list = list()
    para_score_list = list()
    for para_id,para_score in sorted_para_scores[0:5]:
        para_score_list.append(para_score)
        para_text_list.append(tokenizer_output[int(para_id[0])][str(para_id[0])][str(para_id[1])]['text'])

    return para_score_list,para_text_list
